var app = angular.module('ApioApplicationX', ['apioProperty'])
app.controller('defaultControllerX',
  ['$scope', 'currentObject', 'objectService', 'socket', 'sweet', '$http', '$element', 'applicationParameters',
    function ($scope, currentObject, objectService, socket, sweet, $http, $element, applicationParameters) {
      var map
      var marker

      $scope.object = currentObject.get()
      $scope.seconds = '0'
      $scope.originalObject = JSON.parse(JSON.stringify($scope.object))
      setInterval(function () {
        if ($scope.object.properties.datetime == 0) {
          currentObject.update('datetime', String(new Date().getTime()), true, false)
        } else {
          var a = new Date().getTime()
          $scope.seconds = String(parseInt((a - Number($scope.object.properties.datetime)) / 1000))
          $scope.$applyAsync()
        }
      }, 1000)
      $scope.sunshine = ''
      $scope.sunset = ''
      $scope.delay1 = ''
      $scope.delay2 = ''
      $scope.sentRel1 = false
      $scope.sunriseOptions = ''
      $scope.sunsetOptions = ''
      $scope.rawCommand = ''

      $scope.sendCommand = function () {
        console.log('SONO QUELLO CHE INVIO ', $scope.rawCommand)
        //  rawcommand
        currentObject.update('rawcommand', $scope.rawCommand)
      }

      console.log("Sono il defaultController e l'oggetto è = ", $scope.object)

      var first = 0

      // alla chiusura dell'App chiudo le socket
      $scope.$on('$destroy', function () {
        console.log('12, $destroy ----------------------------------------')
        socket.off('apio_server_update')
      })

      // The md-select directive eats keydown events for some quick select
      // logic. Since we have a search input here, we don't need that logic.
      $element.find('input').on('keydown', function (ev) {
        ev.stopPropagation()
      })

      $scope.searchTermMain = ''

      // Function that close the current open App
      $scope.closeApp = function () {
        angular.element(document.getElementsByClassName('topAppApplication')[0]).scope().goBackToHome()
      }

      // variabile che gestisce l'animazione del caricamento. Sarà false quando i dati sono caricati.
      $scope.appNotReady = false
      // istanzio le variabili necessarie all'App Gateway
      $scope.nosim6 = []
      $scope.filteredNoSim6 = []
      $scope.filterNoSim6List = function () {
        // TODO implementa filtro di ricera
        $scope.filteredNoSim6 = $scope.nosim6.filter(function (device) {
          var found = false
          if (device.name.toLowerCase().indexOf($scope.searchTermMain.toLowerCase()) > -1) {
            found = true
          }
          if (device.quadro && device.quadro.name.toLowerCase().indexOf($scope.searchTermMain.toLowerCase()) > -1) {
            found = true
          }
          return found
        })
      }
      $scope.selectedObject = ''

      // prelevo tutti gli oggetti necessari ---> all'avvio saranno necessari solo i gateway
      // objectService.list().then(function (response) {
      //   console.log("---------------------------------------------");
      //   console.log("objectService response = ", response);
      //   response.data.forEach(function (t, number) {
      //     if (t.hasOwnProperty('objectId') && t.objectId === $scope.object.objectId) {
      //       t.nameFinal = "Generale";
      //       $scope.nosim6.unshift(t);
      //     } else if (t.hasOwnProperty('appId') && t.appId === "nosim6" && t.hasOwnProperty('type') && t.type === "instance") {
      //       $scope.nosim6.push(t);
      //     }
      //   });
      //   $scope.nosim6.forEach(function (t, number) {
      //     if (t.hasOwnProperty('parentObjectId')) {
      //       console.log("T ", t);
      //       $scope.nosim6[number].nameFinal = t.name
      //     }
      //   });
      //   console.log("+++++ $scope.gatewayList = ", $scope.gatewayList);
      //   console.log("$scope.objectIdAppNodi = ", $scope.objectIdAppNodi);
      //   console.log("$scope.deviceList = ", $scope.deviceList);
      //   $scope.selectedObject = $scope.nosim6[0];
      //   $scope.appNotReady = false;
      // });

      $scope.loadNoSim6s = function () {
        $http({
          method: 'GET',
          url: 'http://www.apio.cloud/proxy/service/rest_api/objects',
          params: {
            apioId: $scope.object.apioId,
            type: 'instance',
            appId: 'nosim6'
          }
        })
          .then(function (response) {
            console.log('I NOSIM', response)
            $scope.nosim6 = response.data.data
            $scope.filteredNoSim6 = response.data.data

            var initParameters = applicationParameters.get()

            if (initParameters.hasOwnProperty('instanceObjectId')) {
              $scope.selectedObject = $scope.nosim6.find(function (el) {
                return el.objectId === initParameters.instanceObjectId
              })
              console.log('$scope.selectedObject = ', $scope.selectedObject)
              $scope.selectedObjChanged()
            }
          })
          .catch(function (error) {
            console.log('Error', error)
          })
      }
      $scope.loadNoSim6s()

      // funzione che parte al cambiamento di un oggetto selezionato
      $scope.selectedObjChanged = function () {
        console.log('scope.selectedObject = ', $scope.selectedObject)
        $scope.appNotReady = true
        // N.B. Prendo l'objectId anzichè l'address perchè tutte le istanze possono avere objectId!=address.
        objectService.getById($scope.selectedObject.objectId).then(function (response) {
          currentObject.set(response.data)
          // console.log("--------- selectedObjChanged response.data = ", response.data);
          $scope.object = currentObject.get()

          console.log('################# NEW OBJECT = ', $scope.object)
          $scope.loadDeviceMeters()

          if (first == 1) {
            var i2 = setInterval(function () {
              if (map && marker) {
                clearInterval(i2)
                map.panTo(new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long))
                /* map = new google.maps.Map(element, {
                  zoom: 18,
                  center: new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long)
              }); */
                // marker.setPosition(new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long))

                // map.setCenter(new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long));
                marker.setMap(null)
                marker = new google.maps.Marker({
                  position: new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long),
                  map: map
                })
              }
            }, 0)
          } else {
            var element = document.getElementById('nosim6_map')
            element.style.height = '300px'
            if (element) {
              var i1 = setInterval(function () {
                if (google && google.maps) {
                  clearInterval(i1)
                  map = new google.maps.Map(element, {
                    zoom: 18,
                    scrollwheel: false,
                    center: new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long)
                  })

                  marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.object.properties.gps_lat, $scope.object.properties.gps_long),
                    map: map
                  })
                }
              }, 0)
            }
            first = 1
          }
          $scope.modbusInstalled = []
          objectService.list().then(function (data) {
            $scope.allObjects = data.data
            for (var s in data.data) {
              // console.log("data.data[s]", data.data[s]);
              // console.log("$scope.object.address", $scope.object.address);
              if (data.data[s].hasOwnProperty('category') && data.data[s].category === 'modbus' && data.data[s].parentAddress === $scope.object.address) {
                console.log('ESSE********** ', data.data[s].category)
                $scope.modbusInstalled.push(data.data[s])
              }
            }
          })

          // TODO quando l'oggetto selezionato è === originalObject allora non fare niente, se diverso impostare le variabili come scritto sotto

          // il setTimeout serve per essere sicuri che quando aggiorno il currentObject è cambiato
          setTimeout(function () {
            $scope.sunriseOptions = $scope.object.properties.sunriseOptions
            $scope.sunsetOptions = $scope.object.properties.sunsetOptions
            $scope.delay1 = $scope.object.properties.delay1
            $scope.delay2 = $scope.object.properties.delay2

            $('#settingsDataInstallazione').datepicker({
              format: 'dd-mm-yyyy',
              multidate: false
            }).off('changeDate').on('changeDate', function (e) {
              if (e.dates.length === 1) {
                $scope.settingsTable.dataInstallazione.valore = e.date.getDate() + '-' + (e.date.getMonth() + 1) + '-' + e.date.getFullYear()
                console.log('$scope.settingsTable.dataInstallazione.valore = ', $scope.settingsTable.dataInstallazione.valore)
              }
            })
          }, 180)

          $scope.appNotReady = false
          $scope.$broadcast('propertyUpdate')
          $scope.$applyAsync()
        }, 180)
      }

      socket.on('apio_server_update', function (data) {
        // console.log('update', data)
        // console.log("Sono qui >>> ",data)
        if (data.apioId === $scope.object.apioId && data.objectId === $scope.object.objectId) {
          console.log('Update data ', data)
          if (data.properties.hasOwnProperty('rel1')) {
            $scope.switch1 = Number(data.properties.rel1)
          } else if (data.properties.hasOwnProperty('rel2')) {
            $scope.switch2 = Number(data.properties.rel2)
          } else if (data.properties.hasOwnProperty('rel3')) {
            $scope.switch3 = Number(data.properties.rel3)
          } else if (data.properties.hasOwnProperty('rel4')) {
            $scope.switch4 = Number(data.properties.rel4)
          } else if (data.properties.hasOwnProperty('confirmed')) {
            // alert("Tornato")
            console.log('Update data confirmed')
            $scope.sentRel1 = false
          }
        }
      })

      /* $scope.rele = function (data) {
        console.log('Sono qui >>> ', data)
        if (data === 4) {
          console.log($scope.object.properties.rel4)
          currentObject.update('din2', $scope.object.properties.rel4, true, false)
        }
        // $scope.sentRel1 = true
      } */

      $scope.closeApp = function () {
        angular.element(document.getElementsByClassName('topAppApplication')[0]).scope().goBackToHome()
      }

      $scope.allObjects = {}

      $scope.reset = false

      $scope.modbusInstalled = []
      $scope.viewConfirmList = false
      $scope.callbackInstallModbus = true
      $scope.selectedValue = '1'
      $scope.addressModbus = 1
      $scope.nameModbus
      $scope.installedShow = function () {
        if ($scope.modbusInstalled.length > 0) {
          return true
        } else {
          return false
        }
      }

      socket.on('apio_server_new', function (data) {
        console.log('data socket service ', data)
        objectService.getById(data).then(function (data) {
          console.log('apio_server_new', data.data)
          console.log('verifico se è stato specificato un nome: ', $scope.nameModbus)

          $scope.callbackInstallModbus = true

          $scope.addressModbus = 1
          $scope.selectedValue = ''
          $scope.object.properties.modbus = 1

          $scope.nameModbus = document.getElementById('nameModbus').value

          if (typeof $scope.nameModbus !== 'undefined' && $scope.nameModbus !== '') {
            // alert($scope.nameModbus);

            data.data.name = $scope.nameModbus

            var o = {
              address: data.data.address,
              id: data.data.objectId,
              name: data.data.name,
              services: [],
              tag: ''
            }
            console.log('ChangeSettings la richiesta: ', o)

            $http.post('http://www.apio.cloud/apio/app/changeSettings', o).success(function (data) {
              document.getElementById('nameModbus').value = ''
              $scope.nameModbus = ''
              console.log('SUCCES CHANGE NAME', $scope.nameModbus, $scope.callbackInstallModbus)
            })
          } else {
            // alert("Name App campo vuoto!");
          }
          if (data.data.parentAddress === $scope.object.address) {
            $scope.modbusInstalled.push(data.data)
          }
          $scope.allObjects[data.data._id] = data.data
        })
      })

      $scope.deleteApioApplication = function (idModbus, address) {
        // console.log("deleting the application " + $scope.currentApplication.objectId);
        $('#appModal').modal('hide')
        sweet.show({
          title: 'Deleting Application.',
          text: 'Your will not be able to restore those information unless you have them exported!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonClass: 'btn-warning',
          cancelButtonClass: 'btn-info',
          confirmButtonText: 'Delete the App',
          cancelButtonText: 'Keep it',
          closeOnConfirm: false,
          closeOnCancel: true
        }, function (isConfirm) {
          if (isConfirm) {
            $http.post('http://www.apio.cloud/apio/app/delete', { id: idModbus }).success(function (data, status, header) {
              console.log('/apio/app/delete success()')
              // sweet.show("Done!", "Your wizard procedure is done. Proceed to The Apio editor", "success");
              sweet.show({
                title: 'Done!',
                text: 'Your Application is deleted',
                type: 'success',
                showCancelButton: false,
                confirmButtonClass: 'btn-success',
                confirmButtonText: 'Ok',
                closeOnConfirm: true
              }, function () {
                console.log('$scope.modbusInstalled pre ', $scope.modbusInstalled)
                for (var l = 0; l < $scope.modbusInstalled.length; l++) {
                  if ($scope.modbusInstalled[l].objectId === idModbus) {
                    $scope.modbusInstalled.splice(l, 1)
                    $scope.addressModbus = 1
                    $scope.selectedValue = 1
                    $scope.object.properties.modbus = 1
                    $scope.$apply()
                    break
                  }
                }
                console.log('$scope.modbusInstalled post ', $scope.modbusInstalled)
                currentObject.update('remove', address, true, false)
              })
            }).error(function (data, status, header) {
              console.log('/apio/app/delete failure()')
            })
          }
        })
      }

      $scope.removeModbus = function (objectId) {
        if ($scope.object.properties.modbus !== null) {

        }
        console.log('$scope.object ', $scope.object.properties.modbus)
      }

      $scope.addModbus = function (objectId) {
        console.log('$scope.addressModbus ', $scope.addressModbus)
        console.log("document.getElementById('addressModbus').value", document.getElementById('addressModbus').value)
        if (document.getElementById('addressModbus').value !== 0 && document.getElementById('addressModbus').value !== null) {
          console.log('OK!')
          var newAddressApio = '0'
          var tempAddressApio = '0'
          for (var s in $scope.allObjects) {
            if (Number(newAddressApio) < Number($scope.allObjects[s].objectId)) {
              newAddressApio = $scope.allObjects[s].objectId
            }
            console.log('newAddressApio ', newAddressApio)
          }
          newAddressApio = String(Number(newAddressApio) + 1)
          $scope.addressModbus = newAddressApio + '|' + document.getElementById('addressModbus').value
          var property = $scope.object.db.modbus[$scope.object.properties.modbus].split(' ')
          property = property[0] + property[1]
          $scope.addressModbus = property + '|' + $scope.addressModbus
          currentObject.update('newModbus', $scope.addressModbus, true, false)

          document.getElementById('addressModbus').value = ''

          $scope.callbackInstallModbus = false
          $scope.viewConfirmList = false
        } else {
          alert('ATTENZIONE! Inserisci un Address Modbus!')
        }
      }

      $scope.discardModbus = function (objectId) {
        $scope.viewConfirmList = false
        $scope.addressModbus = 1
        document.getElementById('addressModbus').value = ''
        $scope.selectedValue = ''
        $scope.object.properties.modbus = 1
        $scope.$apply()
      }

      $scope.selectDevice = function () {
        console.log('Selected')

        if ($scope.object.properties.modbus !== '1') {
          $scope.selectedValue = $scope.object.db.modbus[$scope.object.properties.modbus]
          $scope.viewConfirmList = true
        } else {
          $scope.viewConfirmList = false
        }
      }

      $scope.resetDIN = function () {
        currentObject.update('reset', '1')
        $scope.reset = true
        setTimeout(function () {
          $scope.reset = false
        }, 5000)
      }

      $scope.sendswitchonoff = function () {
        console.log('$scope.sunriseOptions = ', angular.copy($scope.sunriseOptions))
        console.log('$scope.sunsetOptions = ', angular.copy($scope.sunsetOptions))
        console.log('delay1', angular.copy($scope.delay1))
        console.log('delay2', angular.copy($scope.delay2))

        currentObject.update('sunriseOptions', String($scope.sunriseOptions), true, false)
        currentObject.update('sunsetOptions', String($scope.sunsetOptions), true, false)
        currentObject.update('delay1', $scope.delay1, true, false)
        currentObject.update('delay2', $scope.delay2, true, false)
        // switch_settings la property da aggiornare solo sotto è questa
        var o = {}
        o.sunrise = {
          when: String($scope.sunriseOptions),
          value: $scope.delay1
        }
        o.sunset = {
          when: String($scope.sunsetOptions),
          value: $scope.delay2
        }
        console.log('PPPPPPPPPP - switch_settings = ', o)
        currentObject.update('switch_settings', JSON.stringify(o))
      }

      /* $scope.sendswitchonoff = function () {
      var o = $scope.sunshine;
      if ($scope.delay1.length === 1) {
        $scope.delay1 = "00" + $scope.delay1;
      } else if ($scope.delay1.length === 2) {
        $scope.delay1 = "0" + $scope.delay1;
      }
      o += delay1;
      o += $scope.sunset;
      if ($scope.delay2.length === 1) {
        $scope.delay2 = "00" + $scope.delay2;
      } else if ($scope.delay2.length === 2) {
        $scope.delay2 = "0" + $scope.delay2;
      }
      o += delay2;
      currentObject.update("switchonoff", o, true, false);
    }; */
      $scope.settingProgramDisabled = true
      $scope.changeOnOffSettings = function () {
        console.log('pippo1', $scope.delay1)
        console.log('pippo2', $scope.delay2)
        if ($scope.delay1 <= 240 && $scope.delay2 <= 240) {
          $scope.settingProgramDisabled = false
        } else {
          $scope.settingProgramDisabled = true
        }
      }

      socket.on('apio_server_update', function (data) {
        if (data.objectId === $scope.object.objectId) {
          $scope.reset = false
        }
      })

      // objectService.list().then(function (data) {
      //   $scope.allObjects = data.data
      //   for (var s in data.data) {
      //     // console.log("data.data[s]", data.data[s]);
      //     // console.log("$scope.object.address", $scope.object.address);
      //     if (data.data[s].hasOwnProperty('category') && data.data[s].category === 'modbus' && data.data[s].parentAddress === $scope.object.address) {
      //       console.log('ESSE********** ', data.data[s].category)
      //       $scope.modbusInstalled.push(data.data[s])
      //     }
      //   }
      // })

      $scope.loadDeviceMeters = function (meter) {
        $http({
          method: 'GET',
          url: 'http://www.apio.cloud/proxy/service/rest_api/objects',
          params: {
            apioId: $scope.object.apioId,
            appId: 'SenecaS504C',
            type: 'instance',
            parentAddress: $scope.object.address
          }
        })
          .then(function (response) {
            console.log('I meters', response.data.data)
            $scope.modbusInstalled = response.data.data
          })
          .catch(function (error) {
            console.log('Error', error)
          })
      }

      function loadByAppId (appId) {
        return $http({
          method: 'GET',
          url: 'http://www.apio.cloud/proxy/service/rest_api/objects',
          params: {
            appId: appId,
            apioId: $scope.object.apioId,
            type: 'instance'
          }
        })
      }

      var quadri = []
      var meters = []
      loadByAppId('quadroPI')
        .then(function (response) {
          quadri = response.data.data
          console.log('I Quadri', quadri)
          return loadByAppId('SenecaS504C')
        })
        .then(function (response) {
          meters = response.data.data
          console.log('I Meters', meters)
          // Ricongiungo i quadi ai meters
          quadri.forEach(quadro => {
            meters.forEach(meter => {
              if (quadro.properties.meter.value === meter.objectId) {
                quadro.meter = meter
              }
            })
          })
          quadri.filter(quadro => {
            return quadro.meter
          }).forEach(quadro => {
            $scope.nosim6.forEach(device => {
              if (quadro.meter.parentAddress === device.address) {
                device.quadro = quadro
              }
            })
          })

          $scope.filteredNoSim6 = angular.copy($scope.nosim6)
        })
        .catch(function (error) {
          console.log('Error', error)
        })

      /* new relay variables and functions */
      $scope.switch1 = Number($scope.object.properties.rel1)
      $scope.switch2 = Number($scope.object.properties.rel2)
      $scope.switch3 = Number($scope.object.properties.rel3)
      $scope.switch4 = Number($scope.object.properties.rel4)
      $scope.switchChanged = function (prop, val) {
        // console.log("dentro switchChanged -------");
        console.log('prop = ', prop)
        console.log('val = ', val)
        currentObject.update(prop, String(val))
      }
    }])

setTimeout(function () {
  angular.bootstrap(document.getElementById('ApioApplicationX'), ['ApioApplicationX'])
}, 10)
